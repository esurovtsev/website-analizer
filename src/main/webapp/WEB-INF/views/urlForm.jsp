<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Website analizer</title>
    </head>
    <body>
        <h1>Website analizer</h1>
        
        <!-- Show an error message if there is one available -->
        <c:if test="${not empty errorMessage}">
            <p style="color:red;font-weight:bold;"><c:out value="${errorMessage}"/></p>
        </c:if>
        
        <!-- Show a form with URL field and submit button -->
        <s:url value="/" var="form_url" />
        <form action="${form_url}" method="post" accept-charset="utf-8">
            <fieldset>
                <label for="url">URL to analize:</label>
                <input id="url" name="url" type="text" value="${url}" size="90">
            </fieldset>
            <button type="submit">Submit</button>
        </form>
        
        <!-- Show resulting table with website data if there is one available -->
        <c:if test="${not empty websiteData}">
            <table style="margin-top:15px;" cellpadding="5">
            
            <c:forEach var="item" items="${websiteData}">
                <tr>
                    <th><c:out value="${item.key}"/></th>
                    <td><c:out value="${item.value}"/></td>
                </tr>
            </c:forEach>
            
            </table>
        </c:if>
    </body>
</html>