package esurovtsev.websiteanalizer.service;

import java.util.Map;

import org.jsoup.nodes.Document;

/**
 * Responsible for performing a small parsing task (such as obtaining HTML version) and providing results for that
 * parsing.
 */
public interface ParseProcessor {
    /**
     * Parses given {@link Document} and saves results of parsing into provided Map.
     * 
     * @param source
     *            a document to parse.
     * @param result
     *            a map to save results into. The map must be created before calling the method.
     */
    void parse(Document source, Map<String, String> result);
}
