package esurovtsev.websiteanalizer.service;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.Validate;
import org.apache.log4j.LogMF;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import esurovtsev.websiteanalizer.web.HomeController;

public class AnalizerService {
    /**
     * Responsible for obtaining document from provided URL and parsing it with list of predefined
     * {@link ParseProcessor}
     * 
     * @param urlToProcess
     *            a URL of the website the information for which are collected.
     * @return a map of website data ready to render on the page.
     * @throws IOException
     *             if an I/O error occurs.
     */
    public Map<String, String> assembleWebsiteData(URL urlToProcess) throws IOException {
        Map<String, String> websiteData = new LinkedHashMap<String, String>();
        Document document = getDocument(urlToProcess.toString());

        for (ParseProcessor processor : processors) {
            processor.parse(document, websiteData);
        }

        return websiteData;
    }

    /**
     * Sets processors which will be used for assembling website data.
     * 
     * @param processors
     *            a list of {@link ParseProcessor} objects.
     */
    public void setProcessors(List<ParseProcessor> processors) {
        Validate.notEmpty(processors, "processors collection must be neither null nor empty");
        this.processors = processors;
    }

    Document getDocument(String urlToProcess) throws IOException {
        LogMF.debug(LOG, "Getting document for {0}", urlToProcess);
        return Jsoup.connect(urlToProcess).userAgent("Mozilla").get();
    }

    private List<ParseProcessor> processors;

    private static final Logger LOG = Logger.getLogger(AnalizerService.class);
}
