package esurovtsev.websiteanalizer.service;

import java.util.List;
import java.util.Map;

import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.DocumentType;
import org.jsoup.nodes.Node;
import org.springframework.stereotype.Component;

/**
 * Parses and returns HTML version.
 */
@Component
public class HtmlVersionParser implements ParseProcessor {

    @Override
    public void parse(Document source, Map<String, String> result) {
        Validate.notNull(source, "source must not be null");
        Validate.notNull(result, "result must not be null");

        List<Node> allNodes = source.childNodes();
        for (Node node : allNodes) {
            if (node instanceof DocumentType) {
                DocumentType documentType = (DocumentType) node;
                result.put("html", documentType.toString());
                return;
            }
        }
    }

}
