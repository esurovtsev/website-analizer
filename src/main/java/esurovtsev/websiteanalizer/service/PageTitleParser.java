package esurovtsev.websiteanalizer.service;

import java.util.Map;

import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

/**
 * Parses and returns page title.
 */
@Component
public class PageTitleParser implements ParseProcessor {

    @Override
    public void parse(Document source, Map<String, String> result) {
        Validate.notNull(source, "source must not be null");
        Validate.notNull(result, "result must not be null");
        result.put("title", source.title());
    }

}
