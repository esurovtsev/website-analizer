package esurovtsev.websiteanalizer.service;

import java.util.Map;

import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

@Component
public class HeadingsParser implements ParseProcessor {

    @Override
    public void parse(Document source, Map<String, String> result) {
        Validate.notNull(source, "source must not be null");
        Validate.notNull(result, "result must not be null");

        for (int i = 1; i <= 6; i++) {
            Elements headings = source.select("h" + i);
            result.put("h" + i, "" + headings.size());
        }

    }

}
