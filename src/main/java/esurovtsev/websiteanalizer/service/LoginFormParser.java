package esurovtsev.websiteanalizer.service;

import java.util.Map;

import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

@Component
public class LoginFormParser implements ParseProcessor {

    @Override
    public void parse(Document source, Map<String, String> result) {
        Validate.notNull(source, "source must not be null");
        Validate.notNull(result, "result must not be null");

        Elements forms = source.select("form");
        for (Element form : forms) {
            Elements passwords = form.select("input[type=password]");
            if (!passwords.isEmpty()) {
                result.put("login form", "yes");
                return;
            }
        }
        result.put("login form", "no");
    }

}
