package esurovtsev.websiteanalizer.service;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import org.apache.log4j.LogMF;
import org.apache.log4j.Logger;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

@Component
public class LinksParser implements ParseProcessor {

    @Override
    public void parse(Document source, Map<String, String> result) {
        Validate.notNull(source, "source must not be null");
        Validate.notNull(result, "result must not be null");

        int external = 0;
        int internal = 0;
        Elements links = source.select("a[href]");
        for (Element link : links) {
            String href = link.attr("href");
            if (!link.absUrl("href").isEmpty()) {
                if (isLinkExternal(href)) {
                    external++;
                } else {
                    internal++;
                }
            }
        }

        result.put("internal links count", "" + internal);
        result.put("internal broken links count", "" + internalBroken);
    }

    boolean isLinkExternal(String link) {
        return link.matches("^[a-zA-Z]*:\\/\\/.*");
    }

    private static final Logger LOG = Logger.getLogger(LinksParser.class);
}
