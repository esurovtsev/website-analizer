package esurovtsev.websiteanalizer.web;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import org.apache.log4j.LogMF;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import esurovtsev.websiteanalizer.service.AnalizerService;

/**
 * Handles all requests in this simple app.
 */
@Controller
@RequestMapping(value = { "/" })
public class HomeController {
    @RequestMapping(method = RequestMethod.GET)
    public String showUrlForm(Model model) {
        LOG.debug("Showing URL form");
        model.addAttribute(URL, "");

        return "urlForm";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String processUrl(@RequestParam(value = "url") String url, Model model) {
        LogMF.debug(LOG, "Processing url {0}", url);
        model.addAttribute(URL, url);

        try {
            URL urlToProcess = new URL(url);
            Map<String, String> websiteData = analizerService.assembleWebsiteData(urlToProcess);
            model.addAttribute(WEBSITE_DATA, websiteData);

        } catch (MalformedURLException e) {
            model.addAttribute(ERROR, "The URL you provided is not correct!");
        } catch (IOException e) {
            model.addAttribute(ERROR, "There is an error while getting information from the website: " + e.getMessage());
        }

        return "urlForm";
    }

    @Autowired
    private AnalizerService analizerService;

    private static final String URL = "url";
    private static final String ERROR = "errorMessage";
    private static final String WEBSITE_DATA = "websiteData";

    private static final Logger LOG = Logger.getLogger(HomeController.class);
}
