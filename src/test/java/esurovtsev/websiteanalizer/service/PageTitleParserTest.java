package esurovtsev.websiteanalizer.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.*;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

public class PageTitleParserTest {
    private PageTitleParser sut;

    @BeforeMethod
    public void beforeMethod() {
        sut = new PageTitleParser();
    }

    @Test
    public void parse() {
        Map<String, String> expectedModel = Collections.singletonMap("title", "Mozilla");
        Map<String, String> actualModel = new HashMap<String, String>();

        sut.parse(getDocumentWithTitle("Mozilla"), actualModel);
        assertEquals(expectedModel, actualModel);
    }

    private Document getDocumentWithTitle(String title) {
        StringBuffer html = new StringBuffer();

        html.append("<!DOCTYPE html>");
        html.append("<html lang=\"en\">");
        html.append("<head>");
        html.append("<meta charset=\"UTF-8\" />");
        html.append("<title>" + title + "</title>");
        html.append("<meta name=\"description\" content=\"The latest entertainment news\" />");
        html.append("<meta name=\"keywords\" content=\"hollywood gossip, hollywood news\" />");
        html.append("</head>");
        html.append("<body>");
        html.append("<div id='color'>This is red</div> />");
        html.append("</body>");
        html.append("</html>");

        return Jsoup.parse(html.toString());
    }
}
