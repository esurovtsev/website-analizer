package esurovtsev.websiteanalizer.service;

import static org.testng.Assert.*;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;

public class LinksParserTest {
    private LinksParser sut;

    @BeforeMethod
    public void beforeMethod() {
        sut = new LinksParser();
    }

    @Test
    public void isExternalLinkForExternal() {
        assertTrue(sut.isLinkExternal("http://google.com"));
    }

    @Test
    public void isExternalLinkForInternal() {
        assertFalse(sut.isLinkExternal("/google.com"));
    }
}
