package esurovtsev.websiteanalizer.service;

import static org.mockito.Mockito.*;

import java.net.URL;
import java.util.Arrays;

import org.jsoup.nodes.Document;
import org.mockito.InOrder;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AnalizerServiceTest {
    private AnalizerService sut;

    @BeforeMethod
    public void beforeMethod() throws Exception {
        AnalizerService service = new AnalizerService();
        sut = spy(service);
        doReturn(mock(Document.class)).when(sut).getDocument(anyString());
    }

    @Test
    public void callsProcessorsInCorrectOrder() throws Exception {
        ParseProcessor processor1 = mock(ParseProcessor.class);
        ParseProcessor processor2 = mock(ParseProcessor.class);
        ParseProcessor processor3 = mock(ParseProcessor.class);

        sut.setProcessors(Arrays.asList(processor1, processor2, processor3));
        InOrder inOrder = inOrder(processor1, processor2, processor3);

        sut.assembleWebsiteData(new URL("http://google.com"));

        inOrder.verify(processor1).parse(any(Document.class), anyMap());
        inOrder.verify(processor2).parse(any(Document.class), anyMap());
        inOrder.verify(processor3).parse(any(Document.class), anyMap());
    }
}
